# The sysctl class
#
# @summary Adds and enables sysctl settings on a Linux host
#
# @example The main class should not be called directly.  Use `sysctl::settings`
#   sysctl::settings {'20-no-ipv6':
#      settings => {
#           'net.ipv6.conf.all.disable_ipv6'     => '1',
#           'net.ipv6.conf.default.disable_ipv6' => '1',
#           'net.ipv6.conf.lo.disable_ipv6'      => '1',
#      },
#   }
#
class sysctl (
) {
    exec {'sysctl-reload':
	command => '/usr/sbin/sysctl -p',
	refreshonly => true,
    }
}
