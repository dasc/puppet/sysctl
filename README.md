# sysctl

#### Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with sysctl](#setup)
    * [What sysctl affects](#what-sysctl-affects)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

Manage sysctl settings.  This module will manage files in sysctl.d.

## Setup

### What sysctl affects

Files in the sysctl.d directory will be created with the provided sysctl settings.  The settings will be applied using 'sysctl -p' after they are written to the files.

## Usage

To add settings to disable ipv6, use the following:


```
sysctl::settings {'20-no-ipv6':
   settings => {
        'net.ipv6.conf.all.disable_ipv6'     => '1',
        'net.ipv6.conf.default.disable_ipv6' => '1',
        'net.ipv6.conf.lo.disable_ipv6'      => '1',
   },
}
```

You can explicitly request the settings to be included or not:

```
sysctl::settings {'20-no-ipv6':
   ensure   => 'absent',
   settings => {
        'net.ipv6.conf.all.disable_ipv6'     => '1',
        'net.ipv6.conf.default.disable_ipv6' => '1',
        'net.ipv6.conf.lo.disable_ipv6'      => '1',
   },
}
```

This would accomplish the same thing, but leave the empty settings file in place:

```
sysctl::settings {'20-no-ipv6':
   ensure   => 'present',
   settings => {},
}
```

## Limitations

It is not possible to 'unset' a setting by using `ensure => absent`.  This will simply remove the file and not update any of the live sysctl values.
