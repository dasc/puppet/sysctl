# The sysctl::settings class
#
# @summary Adds and enables sysctl settings on a Linux host
#
# @example Basic usage
#   sysctl::settings {'20-no-ipv6':
#      settings => {
#           'net.ipv6.conf.all.disable_ipv6'     => '1',
#           'net.ipv6.conf.default.disable_ipv6' => '1',
#           'net.ipv6.conf.lo.disable_ipv6'      => '1',
#      },
#   }
#
# @param [Enum['present', 'absent']] ensure
#   Indicates of the settings file should exist or not.  Valid options are 'present' and 'absent'.  Defaults to 'present'.
#
# @param [Hash[String, String]] settings
#   sysctl settings to apply.  The hash keys are the names of the options, and the hash values are the option values.
#
define sysctl::settings (
  Enum['present', 'absent'] $ensure = 'present',
  Hash[String, String] $settings,
) {
    include ::sysctl

    file {"/etc/sysctl.d/${name}.conf":
        ensure => $ensure,
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('sysctl/settings.epp', {
	    settings => $settings,
	}),
        notify => Exec["sysctl-reload-${name}"],
    }

    exec {"sysctl-reload-${name}":
	command => "/usr/sbin/sysctl -p /etc/sysctl.d/${name}.conf",
	refreshonly => true,
    }
}
